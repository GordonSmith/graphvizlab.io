---
defaults: []
flags:
- write
minimums: []
name: bb
types:
- rect
used_by: G
---
Bounding box of drawing in points.
