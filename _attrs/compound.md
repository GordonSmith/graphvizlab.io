---
defaults:
- 'false'
flags:
- dot
minimums: []
name: compound
types:
- bool
used_by: G
---
If true, allow edges between clusters.

See [`lhead`](#d:lhead) and [`ltail`](#d:ltail) below.
