---
defaults:
- '14.0'
flags: []
minimums:
- '1.0'
name: fontsize
types:
- double
used_by: ENGC
---
Font size, [in points](#points), used for text.
