---
defaults:
- '":\t "'
flags: []
minimums: []
name: layersep
types:
- string
used_by: G
---
Specifies the separator characters used to split the [`layers`](#d:layers) attribute into a list of layer names.
